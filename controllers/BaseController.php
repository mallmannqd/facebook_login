<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 10/05/18
 * Time: 21:44
 */

namespace controllers;


use core\Controller;
use Facebook\Facebook;

class BaseController extends Controller
{
    /**
     * @var Facebook
     */
    protected $fb;

    public function __construct()
    {
        $this->fb = new Facebook([
            'app_id' => '211128766341959',
            'app_secret' => '026aadac0d2030621654ea01000676b4',
            'default_graph_version' => 'v2.10'
        ]);
    }

    public function estaLogado()
    {
        if (!isset($_SESSION['fb_access_token']) || empty($_SESSION['fb_access_token'])){
            return false;
        }

        return true;
    }

}