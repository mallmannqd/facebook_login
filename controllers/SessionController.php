<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 10/05/18
 * Time: 21:52
 */

namespace controllers;


class SessionController extends BaseController
{
    public function auth()
    {
        $helper = $this->fb->getRedirectLoginHelper();

        $_SESSION['fb_access_token'] = $helper->getAccessToken();

        $res = $this->fb->get('/me?fields=email,name,id', $_SESSION['fb_access_token']);

        $r = json_decode($res->getBody());

        print_r($r); exit();

        header('Location: ' . BASE_URL);
    }

}