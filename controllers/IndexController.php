<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 16:50
 */

namespace controllers;

use Facebook\Facebook;

class IndexController extends BaseController
{
    public function index()
    {
        if (!$this->estaLogado()){
            header('Location: ' . BASE_URL . 'index/login');
            exit;
        }

        $this->loadTemplate('index/index');
    }

    public function login()
    {
        if ($this->estaLogado()){
            header('Location: ' . BASE_URL);
            exit;
        }

        $helper = $this->fb->getRedirectLoginHelper();

        $permissions = array('email');

        $loginUrl = $helper->getLoginUrl('https://localhost/session/auth', $permissions);

        $dados['loginUrl'] = $loginUrl;

        $this->loadTemplate('index/login', $dados);
    }

}