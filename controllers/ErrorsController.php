<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 18/04/18
 * Time: 08:56
 */

namespace controllers;


use core\Controller;

class ErrorsController extends Controller
{
    public function notFound()
    {
        $this->loadTemplate('errors/404');
    }

}