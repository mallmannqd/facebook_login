<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 17:42
 */

namespace core;


class Controller
{

    public function loadView($viewName, $viewData = array())
    {
        extract($viewData);
        require_once('views/'.$viewName.'.php');
    }

    public function loadTemplate($viewName, $viewData = array())
    {
        require_once 'views/template.php';
    }

    public function loadCache($viewName, $viewData = array())
    {
        $cache = new Cache();

        if ($cache->viewExists($viewName) && $cache->isValid($viewName)){
            $cache->loadViewCache($viewName);
            exit;
        }
        ob_start();
        $this->loadTemplate($viewName, $viewData);
        $html = ob_get_contents();
        ob_end_clean();

        $cache->saveViewCache($viewName, $html);
        echo $html;
    }

    public function showMessage()
    {
        if (isset($_SESSION['message'])){ ?>
            <div class="alert alert-<?=$_SESSION['message']['type']?>">
                <?=$_SESSION['message']['message']?>
            </div>
            <?php
            unset($_SESSION['message']);
        }
    }

    public function flashMessage($type, $message)
    {
        $_SESSION['message'] = [
            'type' => $type,
            'message' => $message
        ];
    }

    public function validaLogin()
    {
        if(!isset($_SESSION['cLogin'])) {
            header('Location: ' . BASE_URL . 'login');
            exit;
        }
    }

}