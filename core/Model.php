<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 09/04/18
 * Time: 17:40
 */

namespace core;


class Model
{
    protected $db;

    public function __construct()
    {
        $config = new Config();
        $this->db = $config->getConnection();
    }

}